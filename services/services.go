package services

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"book_magazin/b_go_api_gateaway/config"
	"book_magazin/b_go_api_gateaway/genproto/catalog_service"
	"book_magazin/b_go_api_gateaway/genproto/order_service"
)

type ServiceManagerI interface {
	AuthorService() catalog_service.AuthorsClient
	BookService() catalog_service.BooksClient
	CategoryService() catalog_service.CategorysClient
	OrderService() order_service.OrdersClient
}

type grpcClients struct {
	authorService   catalog_service.AuthorsClient
	bookService     catalog_service.BooksClient
	categoryService catalog_service.CategorysClient
	orderService    order_service.OrdersClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	// User Service...
	connCatalogService, err := grpc.Dial(
		cfg.CatalogServiceHost+cfg.CatalogServicePort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	// Auth Service...
	connOrderService, err := grpc.Dial(
		cfg.OrderServiceHost+cfg.OrderGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		authorService:   catalog_service.NewAuthorsClient(connCatalogService),
		bookService:     catalog_service.NewBooksClient(connCatalogService),
		categoryService: catalog_service.NewCategorysClient(connCatalogService),
		orderService:    order_service.NewOrdersClient(connOrderService),
	}, nil
}

func (g *grpcClients) AuthorService() catalog_service.AuthorsClient {
	return g.authorService
}

func (g *grpcClients) BookService() catalog_service.BooksClient {
	return g.bookService
}

func (g *grpcClients) CategoryService() catalog_service.CategorysClient {
	return g.categoryService
}

func (g *grpcClients) OrderService() order_service.OrdersClient {
	return g.orderService
}
