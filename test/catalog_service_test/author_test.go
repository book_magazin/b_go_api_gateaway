package catalog_service_test

import (
	"book_magazin/b_go_api_gateaway/genproto/catalog_service"
	"fmt"
	"net/http"
	"sync"
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/test-go/testify/assert"
)

var s int64

func TestAuthor(t *testing.T) {
	s = 0
	wg := &sync.WaitGroup{}

	for i := 0; i < 100; i++ {

		wg.Add(1)
		go func() {
			defer wg.Done()
			id:=createAuthor(t)
			deleteAuthor(t, id)
		}()

	}

	wg.Wait()

	fmt.Println("s: ", s)
}

func createAuthor(t *testing.T) string {
	response := &catalog_service.Author{}

	request := &catalog_service.Author{
		Name: faker.Name(),
	}

	resp, err := PerformRequest(http.MethodPost, "/author", request, response)

	assert.NoError(t, err)

	assert.NotNil(t, resp)

	if resp != nil {
		assert.Equal(t, resp.StatusCode, 201)
	}

	fmt.Println(response)

	return response.String()
}

func deleteAuthor(t *testing.T, id string) string {
	resp, _ := PerformRequest(
		http.MethodDelete,
		fmt.Sprintf("/author/%s", id),
		nil,
		nil,
	)

	assert.NotNil(t, resp)

	if resp != nil {
		assert.Equal(t, resp.StatusCode, 202)
	}

	return ""
}
