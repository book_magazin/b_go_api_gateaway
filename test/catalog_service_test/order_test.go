package catalog_service_test

import (
	"book_magazin/b_go_api_gateaway/genproto/order_service"
	"fmt"
	"net/http"
	"sync"
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/test-go/testify/assert"
)

var d int64

func TestOrder(t *testing.T) {
	s = 0
	wg := &sync.WaitGroup{}

	for i := 0; i < 100; i++ {

		wg.Add(1)
		go func() {
			defer wg.Done()
			id:=createAuthor(t)
			deleteAuthor(t, id)
		}()

	}

	wg.Wait()

	fmt.Println("s: ", d)
}

func createOrder(t *testing.T) string {
	response := &order_service.Order{}

	request := &order_service.CreateOrder{
		BookId:      faker.Date(),
		// Description: faker.D,
	}

	resp, err := PerformRequest(http.MethodPost, "/order", request, response)

	assert.NoError(t, err)

	assert.NotNil(t, resp)

	if resp != nil {
		assert.Equal(t, resp.StatusCode, 201)
	}

	fmt.Println(response)

	return response.String()
}

func deleteOrder(t *testing.T, id string) string {
	resp, _ := PerformRequest(
		http.MethodDelete,
		fmt.Sprintf("/order/%s", id),
		nil,
		nil,
	)

	assert.NotNil(t, resp)

	if resp != nil {
		assert.Equal(t, resp.StatusCode, 202)
	}

	return ""
}