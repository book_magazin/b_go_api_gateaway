package catalog_service_test

import (
	"book_magazin/b_go_api_gateaway/genproto/catalog_service"
	"fmt"
	"net/http"
	"sync"
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/test-go/testify/assert"
)

var c int64

func TestBook(t *testing.T) {
	s = 0
	wg := &sync.WaitGroup{}

	for i := 0; i < 100; i++ {

		wg.Add(1)
		go func() {
			defer wg.Done()
			id := createBook(t)
			deleteBook(t, id)
		}()

	}

	wg.Wait()

	fmt.Println("s: ", s)
}

func createBook(t *testing.T) string {
	response := &catalog_service.GetListBookResponse{}

	request := &catalog_service.Book{
		Name:       faker.Name(),
		// AuthorId:   faker.AuthorId(),
		// CategoryId: faker.CategoryId(),
	}

	resp, err := PerformRequest(http.MethodPost, "/book", request, response)

	assert.NoError(t, err)

	assert.NotNil(t, resp)

	if resp != nil {
		assert.Equal(t, resp.StatusCode, 201)
	}

	fmt.Println(response)

	return response.String()
}

func deleteBook(t *testing.T, id string) string {

	resp, _ := PerformRequest(
		http.MethodDelete,
		fmt.Sprintf("/book/%s", id),
		nil,
		nil,
	)

	assert.NotNil(t, resp)

	if resp != nil {
		assert.Equal(t, resp.StatusCode, 202)
	}

	return ""
}